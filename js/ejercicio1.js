const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){

    Pulsaciones();

});

function Pulsaciones() {

    var edad = document.getElementById('edad').value;
    var pulsosTotales = document.getElementById('pulsaciones');
    const minutosTotales = 365.25 * 24 * 60;
    let pulsos = 0;
  
    if (edad <= 1) {
        pulsos += edad * minutosTotales * ((140 + 160) / 2);
    } else if (edad <= 12) {
        pulsos += minutosTotales * ((140 + 160) / 2);
        pulsos += (edad - 1) * minutosTotales * ((115 + 110) / 2);
    } else if (edad <= 40) {
        pulsos += minutosTotales * ((140 + 160) / 2);
        pulsos += 11 * minutosTotales * ((115 + 110) / 2);
        pulsos += (edad - 12) * minutosTotales * ((80 + 70) / 2);
    } else {
        pulsos += minutosTotales * ((140 + 160) / 2);
        pulsos += 11 * minutosTotales * ((115 + 110) / 2);
        pulsos += 28 * minutosTotales * ((80 + 70) / 2);
        pulsos += (edad - 40) * minutosTotales * ((70 + 60) / 2);
    }
  
    pulsosTotales.innerHTML = pulsos;

}