const btnGenerar = document.getElementById('btnGenerar');
btnGenerar.addEventListener('click', function(){

    Calificaciones();

});

function Calificaciones() {

    var listaCalif = document.getElementById('numeros');
    var reprobadosCant = document.getElementById('reprobados');
    var aprobadosCant = document.getElementById('aprobados');
    var reprobadosProm = document.getElementById('reproProm');
    var aprobadosProm = document.getElementById('aproProm');
    var promGen = document.getElementById('promedio');
    var sumaRepro = 0;
    var sumaApro = 0;
    var sumaGen = 0;
    var reprobados = 0;
    var aprobados = 0;
    var arreglo = [];

    while(listaCalif.options.length>0){
        listaCalif.remove(0);
    }

    for(let con = 0; con < 35; con++) {
        let aleatorio = Math.floor(Math.random()*(11)); 
        listaCalif.options[con] = new Option(aleatorio, 'valor:' + con);
        arreglo[con] = aleatorio;
    }

    for(let con = 0; con < arreglo.length; con++) {
        if(arreglo[con] < 7) {
            sumaRepro += arreglo[con];
            reprobados++;
        }
    }

    for(let con = 0; con < arreglo.length; con++) {
        if(arreglo[con] >= 7) {
            sumaApro += arreglo[con];
            aprobados++;
        }
    }

    for(let con = 0; con < arreglo.length; con++) {
        sumaGen += arreglo[con];
    }

    reprobadosCant.innerHTML = reprobados;
    aprobadosCant.innerHTML = aprobados;

    reprobadosProm.innerHTML = (sumaRepro / reprobados).toFixed(1);
    aprobadosProm.innerHTML = (sumaApro / aprobados).toFixed(1);

    promGen.innerHTML = (sumaGen / 35).toFixed(1);

}
